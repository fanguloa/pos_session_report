odoo.define('pos_session_report.pos_session_report', function(require){
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var rpc = require('web.rpc');
    // var Model = require('web.DataModel');
    var gui = require('point_of_sale.gui');
    var QWeb = core.qweb;
    var SessionReceiptScreenWidget = screens.ReceiptScreenWidget.extend({
        template: 'SessionReceiptScreenWidget',
        click_next: function(){
            this.gui.show_screen('products');
        },
        click_back: function(){
            this.gui.show_screen('products');
        },
        render_receipt: function(){
            self = this;
            rpc.query({
                model: 'report.point_of_sale.report_saledetails',
                method: 'get_pos_sale_details',
                args: [false,false, self.pos.config.id],
            }).then(function(result){
                var env = {
                    widget:self,
                    company: self.pos.company,
                    pos: self.pos,
                    products: result.products,
                    payments: result.payments,
                    taxes: result.taxes,
                    total_paid: result.total_paid,
                    date: (new Date()).toLocaleString(),
                    pos_name:result.pos_name,
                    cashier_name:result.cashier_name,
                    session_start:result.session_start,
                    session_end:result.session_end
                };
                var report = QWeb.render('XMLSaleDetailsReport', env);
                self.$('.pos-receipt-container').html(report);
            });
        },
        print_web: function(){
            window.print();
        },
    });

    gui.define_screen({name:'session-receipt', widget: SessionReceiptScreenWidget});
    var WVPosSessionReportButton = screens.ActionButtonWidget.extend({
        template: 'WVPosSessionReportButton',
        
        print_xml: function(){
            self = this;
            rpc.query({
                model: 'report.point_of_sale.report_saledetails',
                method: 'get_pos_sale_details',
                args: [false,false, self.pos.config.id],
            }).then(function(result){
                var env = {
                    company: self.pos.company,
                    pos: self.pos,
                    products: result.products,
                    payments: result.payments,
                    taxes: result.taxes,
                    total_paid: result.total_paid,
                    date: (new Date()).toLocaleString(),
                    pos_name:result.pos_name,
                    cashier_name:result.cashier_name,
                    session_start:result.session_start,
                    session_end:result.session_end
                };
                var report = QWeb.render('WvSaleDetailsReport', env);
                self.pos.proxy.print_receipt(report);
            });     
        },
        button_click: function(){
            if (!this.pos.config.iface_print_via_proxy) {
                this.gui.show_screen('session-receipt');
            } else {
                this.print_xml();
            }
        },
    });

    screens.define_action_button({
        'name': 'Session Report',
        'widget': WVPosSessionReportButton,
        'condition': function(){
            return this.pos.config.allow_session_receipt;
        },
    });




});
